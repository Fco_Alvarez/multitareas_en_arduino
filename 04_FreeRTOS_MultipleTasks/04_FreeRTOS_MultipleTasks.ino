/* Dr. Carlos Fco. Alvarez Salgado
 * Programa para demostrar funcionamiento de un dispositivo sencillo 
 * en diferentes formas de progrmacion para multiples tareas
 * 
 * 04_FreeRTOS_MultipleTasks: Programacion por tareas en FreeRTOS
 * Ventajas:
 *  Secillo para programar
 *  recursos controlados y administrados por FreeRTOS
 *  Codigo mas robusto
 * Desventajas:
 *  Necesario aprender a progrmar con FreeRTOS
 *  Limitante en el manejo de interrupciones por tiempos de tareas
 *  Mayor consumo de recursos
 *  
 */
 
#include <Arduino_FreeRTOS.h>

// definir tareas
void TaskBlink( void *pvParameters );
void TaskBlink2( void *pvParameters );
void TaskBlink3( void *pvParameters );
void TaskBotonLED( void *pvParameters );

const int pinBoton=2;
const int salidaBoton=3;
const int salida_blink=4;
const int salida_blink2=5;
const int salida_blink3=6;
const int tiempo=(500/portTICK_PERIOD_MS);

void setup() {
  // Configura y corre tareas
  pinMode(salida_blink,OUTPUT);
  pinMode(salida_blink2,OUTPUT);
  pinMode(salida_blink3,OUTPUT);
  pinMode(salidaBoton,OUTPUT);
  pinMode(pinBoton,INPUT_PULLUP);

  xTaskCreate(TaskBlink,"Blink",128,NULL,2,NULL); // Tarea 1
  xTaskCreate(TaskBlink2,"Blink2",128,NULL,2,NULL); // Tarea 2
  xTaskCreate(TaskBlink3,"Blink3",128,NULL,2,NULL); // Tarea 3
  xTaskCreate(TaskBotonLED,"BotonLED",128,NULL,2,NULL); //Tarea 4
}

// Tareas
void TaskBlink( void *pvParameters ){
  for (;;)
  {
    digitalWrite(salida_blink,HIGH);
    vTaskDelay( tiempo);
    digitalWrite(salida_blink,LOW);
    vTaskDelay( tiempo);
  }
}

void TaskBotonLED( void *pvParameters ){
  for (;;)
  {
    digitalWrite(salidaBoton,!digitalRead(pinBoton));
  }
}

void TaskBlink2( void *pvParameters ){
  for (;;)
  {
    digitalWrite(salida_blink2,HIGH);
    vTaskDelay( tiempo/2);
    digitalWrite(salida_blink2,LOW);
    vTaskDelay( tiempo/2);
  }
}

void TaskBlink3( void *pvParameters ){
    for (;;)
    {
      digitalWrite(salida_blink3,HIGH);
      vTaskDelay( tiempo/3);
      digitalWrite(salida_blink3,LOW);
      vTaskDelay( tiempo/3);
    }
}

void loop() {
  // Nada que hacer.


}
