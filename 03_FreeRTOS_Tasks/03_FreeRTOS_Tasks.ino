/* Dr. Carlos Fco. Alvarez Salgado
 * Programa para demostrar funcionamiento de un dispositivo sencillo 
 * en diferentes formas de progrmacion para multiples tareas
 * 
 * 03_FreeRTOS_Task: Programacion por tareas en FreeRTOS
 * Ventajas:
 *  Secillo para programar
 *  recursos controlados y administrados por FreeRTOS
 *  Codigo mas robusto
 * Desventajas:
 *  Necesario aprender a progrmar con FreeRTOS
 *  Limitante en el manejo de interrupciones por tiempos de tareas
 *  Mayor consumo de recursos
 *  
 */
 
#include <Arduino_FreeRTOS.h>

// definir tareas
void TaskBlink( void *pvParameters );
void TaskBotonLED( void *pvParameters );

const int pinBoton=2;
const int salidaBoton=3;
const int salida_blink=4;
const int tiempo=(500/portTICK_PERIOD_MS);

void setup() {
  // Configura y corre tareas
  pinMode(salida_blink,OUTPUT);
  pinMode(salidaBoton,OUTPUT);
  pinMode(pinBoton,INPUT_PULLUP);

  xTaskCreate(TaskBlink,"Blink",128,NULL,2,NULL); // Tarea 1
  xTaskCreate(TaskBotonLED,"BotonLED",128,NULL,2,NULL); //Tarea 2
}

// Tareas
void TaskBlink( void *pvParameters ){
  for (;;)
  {
    digitalWrite(salida_blink,HIGH);
    vTaskDelay( tiempo);
    digitalWrite(salida_blink,LOW);
    vTaskDelay( tiempo);
  }
}

void TaskBotonLED( void *pvParameters ){
  for (;;)
  {
    digitalWrite(salidaBoton,!digitalRead(pinBoton));
  }
}

void loop() {
  // Nada que hacer.
}
