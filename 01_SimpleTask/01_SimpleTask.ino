/* Dr. Carlos Fco. Alvarez Salgado
 * Programa para demostrar funcionamiento de un dispositivo sencillo 
 * en diferentes formas de progrmacion para multiples tareas
 * 
 * 01_SimpleTask: Programacion Lineal y secuencial (MultiProgramacion)
 * Ventajas:
 *  Secillo para programar
 *  recursos controlados y administrados por el programador
 *  Tamaño de codigo peuqeño
 * Desventajas:
 *  El programador necesita administrar los tiempos de cada evento
 *  Es dificilmente escalable en proyectos grandes
 *  interaccion con el usuario puede ser no muy amigable
 *  
 */
const int pinBoton=2;
const int salidaBoton=3;
const int salida_blink=4;
const int tiempo=500;

void setup() {
  pinMode(salida_blink,OUTPUT);
  pinMode(salidaBoton,OUTPUT);
  pinMode(pinBoton,INPUT_PULLUP);
}



void loop() {
  // Tareas en Multiprogramacion
  
  // Tarea 1: Encender y Apagar led por tiempo
  digitalWrite(salida_blink,HIGH);
  delay(tiempo);
  digitalWrite(salida_blink,LOW);
  delay(tiempo);  
  
  // Tarea 2: controlar la entrada con el boton 0
  digitalWrite(salidaBoton,!digitalRead(pinBoton));
}
