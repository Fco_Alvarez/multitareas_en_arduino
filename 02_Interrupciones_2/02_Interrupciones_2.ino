/* Dr. Carlos Fco. Alvarez Salgado
 * Programa para demostrar funcionamiento de un dispositivo sencillo 
 * en diferentes formas de programacion para multiples tareas
 * 
 * 02_Interrupciones_2: Programacion Lineal y secuencial con interrupciones
 * todas las tareas y eventos en interrupciones por evento o tiempo
 * Ventajas:
 *  Secillo para programar
 *  recursos controlados y administrados por el programador
 *  Tamaño de codigo pequeño
 * Desventajas:
 *  El programador necesita administrar los tiempos de cada evento
 *  Es dificilmente escalable en proyectos grandes
 *  interaccion con el usuario puede ser no muy amigable
 *  Limitante en el manejo de interrupciones por tiempos de tareas
 *  
 */

#include <TimerOne.h>
const int pinBoton=2;
const int salidaBoton=3;
const int salida_blink=4;
const int tiempo=500; // 500ms
bool estado=false;

void atencion_de_interrupcion(){
  // Tarea 2: controlar la entrada 3 con el boton 0
    digitalWrite(salidaBoton,!digitalRead(pinBoton));
}

void setup() {
  pinMode(salidaBoton,OUTPUT);
  pinMode(salida_blink,OUTPUT);
  pinMode(pinBoton,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinBoton), atencion_de_interrupcion, CHANGE);
  Timer1.initialize(tiempo);       
  Timer1.attachInterrupt(atencion_de_interrupcion_x_tiempo); // Activa la interrupcion y la asocia
}

void atencion_de_interrupcion_x_tiempo(){
  // Tarea 1: Encender y Apagar led en pt4 por tiempo
  estado=!estado;  
}

void loop() {
  // Tareas en Interrupciones de Hardware  
  digitalWrite(salida_blink,estado);
}
