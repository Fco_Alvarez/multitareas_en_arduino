[![FCQI](./.images/FCQI_Logo.jpg "FCQI")](http://FCQI.tij.uabc.mx "FCQI")
# Trabajando Multiples tareas en Arduino:
Dr. Carlos Fco. Alvarez Salgado

Materia: Sistemas Operativos

[Facultad de Ciencias Quimicas e Ingenieria](http://fcqi.tij.uabc.mx "Facultad de Ciencias Quimicas e Ingenieria")

[Universidad Autonoma de Baja California](http://UABC.MX "Universidad Autonoma de Baja California")

Unidad Tijuana

Estos archivos son base para la demostracion de diferentes formas de trabajar con multiples tareas usando un ***Arduino UNO*** o compatible.

## Contenido:
Cada practica presenta una forma de trabajar con multiples tareas en un Arduino UNO.
###### En este material se encuentra:
- Diagrama de proyecto
- Archivo .hex de firmware
- los archivos INO para la programacion con Arduino IDE.
- Archivo base para simulacion con Proteus VSM

## Practicas:
- 01_SimpleTask
- 02_Interrupciones
- 03_FreeRTOS_Task
- 04_FreeRTOS_MultipleTask

## Dependencias:
- Arduino IDE
-- Librerias FreeRTOS
- Arduino UNO o Simulador Proteus VSM

## Diagrama
![Diagrama base Arduino UNO](./.images/ArduinoMultiTask.jpg "Diagrama base Arduino UNO")