/* Dr. Carlos Fco. Alvarez Salgado
 * Programa para demostrar funcionamiento de un dispositivo sencillo 
 * en diferentes formas de progrmacion para multiples tareas
 * 
 * 02_Interrupciones: Programacion Lineal y secuencial con interrupciones
 * Ventajas:
 *  Secillo para programar
 *  recursos controlados y administrados por el programador
 *  Tamaño de codigo pequeño
 * Desventajas:
 *  El programador necesita administrar los tiempos de cada evento
 *  Es dificilmente escalable en proyectos grandes
 *  interaccion con el usuario puede ser no muy amigable
 *  Limitante en el manejo de interrupciones por tiempos de tareas
 *  
 */
const int pinBoton=2;
const int salidaBoton=3;
const int salida_blink=4;
const int tiempo=500;

void atencion_de_interrupcion(){
  // Tarea 2: controlar la entrada 3 con el boton 0
    digitalWrite(salidaBoton,!digitalRead(pinBoton));
}

void setup() {
  pinMode(salidaBoton,OUTPUT);
  pinMode(salida_blink,OUTPUT);
  pinMode(pinBoton,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinBoton), atencion_de_interrupcion, CHANGE);
}



void loop() {
  // Tareas en Interrupciones de Hardware
  
  // Tarea 1: Encender y Apagar led 4 por tiempo
  digitalWrite(salida_blink,HIGH);
  delay(tiempo);
  digitalWrite(salida_blink,LOW);
  delay(tiempo);
}
